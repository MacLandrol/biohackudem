$(document).ready(function() {

  if(!(/Android|iPhone|iPad|iPod|BlackBerry|Windows Phone/i).test(navigator.userAgent || navigator.vendor || window.opera)){
    skrollr.init({
      forceHeight: false
    });
  }

  $('.ui.form')
      .form({
        firstName: {
          identifier  : 'firstname',
          rules: [
            {
              type   : 'empty',
              prompt : 'Entrez un Prénom svp'
            }
          ]
        },

        lastname: {
          identifier  : 'lastname',
          rules: [
            {
              type   : 'empty',
              prompt : 'Entrez un Nom svp'
            }
          ]
        },

        email: {
          identifier  : 'email',
          rules: [
            {
              type   : 'email',
              prompt : 'Entrez une adresse email valide'
            }
          ]
        }
      }

      ,{
        on: 'blur',
        onSuccess: submitForm
      })
    ;

  $('#main')
    .transition('fade up in', 1500);

  $('.logo')
      .transition('pulse');

  $("#info-section").click(function() {
    $('html, body').animate({
      scrollTop: $("#info").offset().top
    }, 1000);
  });


$('.mapembed').click(function () {
      $('.mapembed iframe').css("pointer-events", "auto");
  });


function getFieldValue(fieldId) { 
      // This is from http://stackoverflow.com/questions/19482258/how-to-submit-a-form-in-semantic-ui
      return $('.ui.form').form('get field', fieldId).val();
   }

//Handle form submission
function submitForm() {
      var formData = {
          lastname: getFieldValue('lastname'),
          firstname: getFieldValue('firstname'),
          affiliation: getFieldValue('affiliation'),
          email: getFieldValue('email')
      };

      $.ajax({ type: 'POST', url: 'submit.php', data: formData, success: onFormSubmitted });
   }

   // Handle post response
function onFormSubmitted(response) {
  userdata= jQuery.parseJSON(response);
    $('#userid').html(userdata.id);
    $('#regisform').hide();
    $('#regisresponse').fadeIn('fast');

   }

});

