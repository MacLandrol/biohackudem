<?php

// Promocode of weemss. Hardcoding it -_-
$promocode = array(
"M1Y10C",
"SGIZD1",
"XBEG2T",
"G8ABOA",
"6PE6AR",
"8111GC",
"S9QCEV",
"36LCZT",
"YRAJ61",
"GPR25L",
"A0KXQ6",
"YPWRJJ",
"5XCMPK",
"NF54VM",
"YYE0RK",
"4UIFS8",
"AFCCLP",
"Q4SK37",
"ATSSZK",
"JMAESR",
"GS9XQH",
"FOOC1H",
"381HQK",
"7Y6B4J",
"PTC66M",
"PYVF4L",
"QSM9JU",
"9M1IUG",
"WONQDM",
"AHC7KK",
"5FIZSD",
"ALZVWE",
"WFONB9",
"HCD1NF",
"IE0URC",
"GX3HYV",
"SG0BRZ",
"IPXB97",
"8C5A4V",
"PKOV96"
);

// Connect to MySQL
$connect = mysql_connect('localhost', 'aebinum', 'aebinumhackathon');

if (!$connect) {
    die('Could not connect: ' . mysql_error($connect));
}

$get_db = mysql_select_db('biohackudem', $connect);

// Create Database if it does not exist
if (!$get_db) {
  $sql = 'CREATE DATABASE biohackudem';

  if (! mysql_query($sql, $connect)) {
      die('Error accessing database: ' . mysql_error($connect) . "\n");
  }
  mysql_select_db('biohackudem', $connect);

}

//Create table if it does not exist

$table_sql = "CREATE TABLE IF NOT EXISTS `hackers` (
   `id` int(6) unsigned NOT NULL auto_increment primary key,
   `firstname` varchar(255) NOT NULL default '',
   `lastname` varchar(255) NOT NULL default '',
   `email` varchar(255) NOT NULL default '',
   `ticketcode` varchar(255) ,
   `udem` tinyint(1) NOT NULL default 0,
   `userid` varchar(255) NOT NULL default ''
   ) DEFAULT CHARSET=utf8";


if (!mysql_query($table_sql, $connect)) {
    die('Could not access table : ' . mysql_error($connect));
}

// Here we are going to get data send by ajax
$firstname = mysql_real_escape_string(_POST['firstname']);
$lastname = mysql_real_escape_string($_POST['lastname']);
$email = mysql_real_escape_string($_POST['email']);
$affiliation = mysql_real_escape_string($_POST['affiliation']);

// uniq hash for user id
$md5seed = $email.$lastname;
$userid = empty($md5seed)? "" :substr(md5($md5seed), 0, 4);
$select_statement = "SELECT ticketcode FROM hackers WHERE udem=1";
$promo_rows = mysql_query($select_statement, $connect);

if(! $promo_rows )
{
  die('Could not get data: ' . mysql_error($connect));
}

// get actual promo index
$promo_ind = mysql_num_rows($promo_rows);

//check is user is from udem
$user_from_udem = "" || strpos($email, 'umontreal.ca', strlen($email) - strlen('umontreal.ca')) !== FALSE ? TRUE : FALSE;

// Only udem students have access to promocode
$user_promo = $user_from_udem ? $promocode[$promo_ind] : "";

// register a new participant
$insert_statement = "INSERT INTO hackers ".
       "(firstname, lastname, email, ticketcode, udem, userid) ".
       "VALUES".
       "('$firstname', '$lastname', '$email', '$user_promo', '$user_from_udem', '$userid')";

$insert_res = mysql_query( $insert_statement, $connect );

if(! $insert_res )
{
  die('Could not enter data: ' . mysql_error($connect));
}

mysql_close($connect);

// Send output back to ajax
$output = array(
	"id" => $userid
	);


$from = 'aebinum-membres@listes.umontreal.ca';
$subject = 'Pré-inscription réussie!';
$message = '<html><body>' .
	'<p>Cher(ère) '.$firstname. ',</p><br>Votre pré-inscription est terminé.' .
	'<p>Vous pouvez acheter votre ticket dès maintenant à cette adresse : <a href = "https://event.gg/657-biohackudem">https://event.gg/657-biohackudem</a></p>.'.
	$user_from_udem ? "<p>En tant qu'étudiant de l'UdeM, <strong>votre ticket est gratuit</strong>. Utilisez ce code lors de votre achat pour obtenir la réduction de 100% (Chercher 'Enter promo code' sur la page): </p> <p style='color:#FFF; background-color:#000; text-align:center; font-size: 1.2em;'>".$user_promo."</p>":"";

$message .= "<br>Au plaisir de vous voir le 2 Mars !<br><p>L'équipe de Biohack UdeM </p>"



$headers = 'From: webmaster@biohackudem.com' . "\r\n" .
    'Reply-To: noreply@biohackudem.com' . "\r\n" .
    'X-Mailer: PHP/' . phpversion();
$headers .= "MIME-Version: 1.0\r\n";
$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
 
if (filter_var($email, FILTER_VALIDATE_EMAIL)) { // this line checks that we have a valid email address
	mail($email, $subject, $message, $header) or die('Error sending Mail'); //This method sends the mail.
}

echo json_encode($output);

?>